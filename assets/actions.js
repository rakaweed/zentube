$(document).ready(function(){
    
    let webApi = window.pywebview;
    let api = webApi.api;
    let btnToggleFullscreen = $("#toggleFullscreen");
    let formYouTubeSearch = $("#searchForm");
    let inputYouTubeSearch = $("#searchInput");
    let contentResultsBox = $("#resultsBox");
    let contentBox = $("#contentBox");
    let btnClose = $("#closeBtn");
    let btnMinimize = $("#minimizeBtn");
    let minimized = false;
    let nextPageToken = null;

    let playContent = function(contentKind, contentId) {
        if (contentKind=="video") {
            return '<iframe src="https://www.youtube.com/embed/' + contentId + '?&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
        } else if (contentKind=="playlist") {
            return '<iframe src="http://www.youtube.com/embed/videoseries?list=' + contentId + '&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
        }
        return 'Nothing to show';
    }

    let resizeIFrame = function() {
        contentBox.find(".content iframe").css({'height': contentBox.find(".content iframe").width()*(9/16)});
        contentBox.css({'height': contentBox.width()*(9/16)});
        let iFrame = contentBox.find(".content iframe");
        iFrame.css({'height': iFrame.width()*(9/16)});
    }
    
    let loadingBox = function() {
        // return '<div class="row"><div class="col s12 center-align"><i class=""></i></div></div>';
        return '<div class="row loading_box">\
                    <div class="col s12 center-align">\
                        <div class="preloader-wrapper active">\
                            <div class="spinner-layer spinner-red-only">\
                                <div class="circle-clipper left">\
                                    <div class="circle"></div>\
                                </div>\
                                <div class="gap-patch">\
                                    <div class="circle"></div>\
                                </div>\
                                <div class="circle-clipper right">\
                                    <div class="circle"></div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>';
    }

    let loadNew = function() {
        let searchQuery = inputYouTubeSearch.val().trim();
        let prevSearchQuery = inputYouTubeSearch.data('prevSearchQuery');
        if(searchQuery && (contentResultsBox.html()=="" || searchQuery!==prevSearchQuery)) {
            contentResultsBox.html(loadingBox());
            let args = {q: searchQuery, maxResults: 10};
            api.yt_search(args).then(function (content) {
                inputYouTubeSearch.data('prevSearchQuery', searchQuery);
                contentResultsBox.html(content);
            });
        }
    }

    let loadMore = function() {
        let prevSearchQuery = inputYouTubeSearch.data('prevSearchQuery');
        if(prevSearchQuery) {
            $(document).find('.load_more').closest('.row').remove();
            contentResultsBox.append(loadingBox());
            let args = {q: prevSearchQuery, maxResults: 10, pageToken: nextPageToken};
            api.yt_search(args).then(function (content) {
                $(document).find('.loading_box').remove();
                contentResultsBox.append(content);
            });
        }
    }

    btnToggleFullscreen.on('click', function() {
        if(this.innerHTML=="fullscreen"){
            this.innerHTML = "fullscreen_exit";
        } else {
            this.innerHTML = "fullscreen";
        }

        api.toggleFullscreen();
    });

    formYouTubeSearch.on('submit', function(e) {
        e.preventDefault();
        loadNew();
    });

    $(document).on('click', '.play_content', function(e) {
        minimized = false;
        let playbox = $(this).closest(".playbox");
        let contentKind = playbox.data('kind');
        let contentId = playbox.data('id');
        // let channelId = playbox.data('channelId');
        contentBox.css({'position': 'absolute', 'top': '0', 'right': '0', 'width':'100%'});
        contentResultsBox.hide();
        let iFrame = contentBox.find(".content iframe");
        if (!iFrame.data('currentId') || iFrame.data('currentId')!=contentId) {
            contentBox.find(".content").html(playContent(contentKind, contentId));
            iFrame = contentBox.find(".content iframe");
            iFrame.data('currentId', contentId);
        }
        window.setTimeout(function(){
            resizeIFrame();
        }, 400);
    });

    btnClose.on('click', function(e) {
        contentBox.css({'right': '150%'});
        contentResultsBox.find(".playbox").css({'background': 'none'});
        window.setTimeout(function(){
            contentResultsBox.show();
            contentBox.find(".content").html("");
        }, 400);
    });
    
    btnMinimize.on('click', function(e) {
        if(minimized) {
            contentResultsBox.hide();
            contentBox.css({'position': 'absolute', 'top': '0', 'right': '0', 'width': '100%'});
        } else {
            contentResultsBox.find(".playbox").css({'background': 'none'});
            contentResultsBox.show();
            contentBox.css({'position': 'fixed', 'top': '20%', 'right': '10%', 'width': '25%'});
            let iFrame = contentBox.find(".content iframe");
            let currentPlaying = contentResultsBox.find(".playbox[data-id='" + iFrame.data('currentId') + "']");
            currentPlaying.css({'background': 'rgb(37, 37, 37)'});
            window.scrollTo(0, currentPlaying.offset().top - 100);
        }
        minimized = !minimized;
        window.setTimeout(function(){
            resizeIFrame();
        }, 400);
    });

    contentBox.on('mouseover', function(){
        $(".material-icons-holders").css({'visibility': 'visible'});
    });

    contentBox.on('mouseout', function(){
        $(".material-icons-holders").css({'visibility': 'hidden'});
    });

    $(document).on('click', '.load_more', function(e) {
        nextPageToken = $(e.target).data('nextPageToken');
        loadMore();
    });

    $(document).on('click', '.retry_connection', function(e) {
        contentResultsBox.find(".retry_connection").closest(".row").remove();
        api.try_connection().then(function(){
            if(contentBox.find(".playbox")) {
                loadMore();
            } else {
                loadNew();
            }
        });
    });

    window.onresize = function() {
        resizeIFrame();
    }
});
