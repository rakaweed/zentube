{
    'kind': 'youtube#searchListResponse', 
    'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/c3WC7ZO5E1VByEcxYcY2ohgmkMg"', 
    'nextPageToken': 'CAoQAA',
    'regionCode': 'NP',
    'pageInfo':
    {
        'totalResults': 88922,
        'resultsPerPage': 10
    },
    'items': 
    [
        {
            'kind': 'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/_pB7fyM2YR9ZMCUYdcGj81o4gzk"',
            'id':
            {
                'kind': 'youtube#channel',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw'
            },
            'snippet':
            {
                'publishedAt': '2008-02-04T16:09:31.000Z',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
                'title': 'thenewboston',
                'description': 'Tons of sweet computer related tutorials and some other awesome videos too!',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://yt3.ggpht.com/--n5ELY2uT-U/AAAAAAAAAAI/AAAAAAAAAAA/d9JvaIEpstw/s88-c-k-no-mo-rj-c0xffffff/photo.jpg'
                    },  
                    'medium':
                    {
                        'url': 'https://yt3.ggpht.com/--n5ELY2uT-U/AAAAAAAAAAI/AAAAAAAAAAA/d9JvaIEpstw/s240-c-k-no-mo-rj-c0xffffff/photo.jpg'
                    },
                    'high':
                    {
                        'url': 'https://yt3.ggpht.com/--n5ELY2uT-U/AAAAAAAAAAI/AAAAAAAAAAA/d9JvaIEpstw/s800-c-k-no-mo-rj-c0xffffff/photo.jpg'
                    }
                },
                'channelTitle': 'thenewboston',
                'liveBroadcastContent': 'none'
            }
        }, 
        {
            'kind':
            'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/XKKwzv3FZaGMz_GVOTw3eJ_nnU0"',
            'id':
            {
                'kind': 'youtube#playlist',
                'playlistId': 'PL6gx4Cwl9DGAKIXv8Yr6nhGJ9Vlcjyymq'
            },
            'snippet':
            {
                'publishedAt': '2014-08-04T15:12:12.000Z',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
                'title': 'C Programming Tutorials',
                'description': 'Official playlist for thenewboston C Programming Tutorials!',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://i.ytimg.com/vi/2NWeucMKrLI/default.jpg',
                        'width': 120,
                        'height': 90
                    },
                    'medium':
                    {
                        'url': 'https://i.ytimg.com/vi/2NWeucMKrLI/mqdefault.jpg',
                        'width': 320,
                        'height': 180
                    },
                    'high':
                    {
                        'url': 'https://i.ytimg.com/vi/2NWeucMKrLI/hqdefault.jpg',
                        'width': 480,
                        'height': 360
                    }
                },
                'channelTitle': 'thenewboston',
                'liveBroadcastContent': 'none'
            }
        },
        {
            'kind': 'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/5yGAyF4sfV34k0EBRkpq2bctAVA"',
            'id':
            {
                'kind': 'youtube#playlist',
                'playlistId': 'PLFE2CE09D83EE3E28'
            },
            'snippet':
            {
                'publishedAt': '2009-05-16T16:59:11.000Z',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
                'title': 'Java (Beginner) Programming Tutorials',
                'description': 'Here are all of my Java programming tutorials.',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://i.ytimg.com/vi/Hl-zzrqQoSE/default.jpg',
                        'width': 120,
                        'height': 90
                    },
                    'medium':
                    {
                        'url': 'https://i.ytimg.com/vi/Hl-zzrqQoSE/mqdefault.jpg',
                        'width': 320,
                        'height': 180
                    },
                    'high':
                    {
                        'url': 'https://i.ytimg.com/vi/Hl-zzrqQoSE/hqdefault.jpg',
                        'width': 480,
                        'height': 360
                    }
                },
                'channelTitle': 'thenewboston',
                'liveBroadcastContent': 'none'
            }
        },
        {
            'kind': 'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/NSmyPyPBLDVxh5PseQb0RkR8tn4"',
            'id':
            {
                'kind': 'youtube#playlist',
                'playlistId': 'PL6gx4Cwl9DGAcbMi1sH6oAMk4JHw91mC_'
            },
            'snippet':
            {
                'publishedAt': '2014-08-26T19:42:39.000Z',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
                'title': 'Python 3.4 Programming Tutorials',
                'description': 'Official playlist for thenewboston Python 3.4 Programming Tutorials!',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://i.ytimg.com/vi/HBxCHonP6Ro/default.jpg',
                        'width': 120,
                        'height': 90
                    },
                    'medium':
                    {
                        'url': 'https://i.ytimg.com/vi/HBxCHonP6Ro/mqdefault.jpg',
                        'width': 320,
                        'height': 180
                    },
                    'high':
                    {
                        'url': 'https://i.ytimg.com/vi/HBxCHonP6Ro/hqdefault.jpg',
                        'width': 480,
                        'height': 360
                    }
                },
                'channelTitle': 'thenewboston',
                'liveBroadcastContent': 'none'
            }
        },
        {
            'kind': 'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/KJ5DNOnlpKI7D1TP2gwgBqy5U1A"',
            'id':
            {
                'kind': 'youtube#playlist',
                'playlistId': 'PLAE85DE8440AA6B83'
            },
            'snippet':
            {
                'publishedAt': '2011-09-25T05:01:17.000Z',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
                'title': 'C++ Programming Tutorials Playlist',
                'description': 'thenewboston Official Buckys C++ Programming Tutorials Playlist!',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://i.ytimg.com/vi/tvC1WCdV1XU/default.jpg',
                        'width': 120,
                        'height': 90
                    },
                    'medium':
                    {
                        'url': 'https://i.ytimg.com/vi/tvC1WCdV1XU/mqdefault.jpg',
                        'width': 320,
                        'height': 180
                    },
                    'high':
                    {
                        'url': 'https://i.ytimg.com/vi/tvC1WCdV1XU/hqdefault.jpg',
                        'width': 480,
                        'height': 360
                    }
                },
                'channelTitle': 'thenewboston',
                'liveBroadcastContent': 'none'
            }
        },
        {
            'kind': 'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/G1NIAwqgVxN101vKAAaB15pFUTA"',
            'id':
            {
                'kind': 'youtube#playlist',
                'playlistId': 'PLC1322B5A0180C946'
            },
            'snippet':
            {
                'publishedAt': '2011-09-25T05:21:14.000Z',
                'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
                'title': 'XHTML and CSS Tutorials Playlist',
                'description': 'thenewboston Official XHTML and CSS Tutorials Playlist!',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://i.ytimg.com/vi/cqszz_OfAFQ/default.jpg',
                        'width': 120,
                        'height': 90
                    },
                    'medium':
                    {
                        'url': 'https://i.ytimg.com/vi/cqszz_OfAFQ/mqdefault.jpg',
                        'width': 320,
                        'height': 180
                    },
                    'high':
                    {
                        'url': 'https://i.ytimg.com/vi/cqszz_OfAFQ/hqdefault.jpg',
                        'width': 480,
                        'height': 360
                    }
                },
                'channelTitle': 'thenewboston',
                'liveBroadcastContent': 'none'
            }
        },
        {
        'kind': 'youtube#searchResult',
        'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/6EXEukKElk-qb3v6MfosJ5NkCWE"',
        'id':
        {
        'kind': 'youtube#video',
        'videoId': 'xsInBQ3LaqM'
        },
        'snippet':
        {
        'publishedAt': '2017-06-05T02:06:21.000Z',
        'channelId': 'UCfV36TX5AejfAGIbtwTc7Zw',
        'title': 'Why Did TheNewBoston Leave YouTube?',
        'description': 'SPONSORS ◅ DevMountain Coding Bootcamp https://goo.gl/P4vgKS Description: In this video we will look at the top 10 server side frameworks in 2019.',
        'thumbnails':
        {
        'default':
        {
        'url': 'https://i.ytimg.com/vi/xsInBQ3LaqM/default.jpg',
        'width': 120,
        'height': 90
        },
        'medium':
        {
        'url': 'https://i.ytimg.com/vi/xsInBQ3LaqM/mqdefault.jpg',
        'width': 320,
        'height': 180
        },
        'high':
        {
        'url': 'https://i.ytimg.com/vi/xsInBQ3LaqM/hqdefault.jpg',
        'width': 480,
        'height': 360
        }
        },
        'channelTitle': 'Chris Hawkes',
        'liveBroadcastContent': 'none'
        }
        },
        {
        'kind': 'youtube#searchResult',
        'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/RB-xpWTNFzS9Wsg0WtZeAA4RJN0"',
        'id':
        {
        'kind': 'youtube#playlist',
        'playlistId': 'PL17E300C92CE0261A'
        },
        'snippet':
        {
        'publishedAt': '2012-08-10T23:53:43.000Z',
        'channelId': 'UCf-4P24f3x4xdxiDsemzc2Q',
        'title': "thenewboston's Java(Beginner) Programming Tutorials Series",
        'description': "This playlist contains all 87 video of thenewboston's(Bucky) Java (Beginner) Programming Tutorial Series. thenewboston's YouTube: ...",
        'thumbnails':
        {
        'default':
        {
        'url': 'https://i.ytimg.com/vi/Hl-zzrqQoSE/default.jpg',
        'width': 120,
        'height': 90
        },
        'medium':
        {
        'url': 'https://i.ytimg.com/vi/Hl-zzrqQoSE/mqdefault.jpg',
        'width': 320,
        'height': 180
        },
        'high':
        {
        'url': 'https://i.ytimg.com/vi/Hl-zzrqQoSE/hqdefault.jpg',
        'width': 480,
        'height': 360
        }
        },
        'channelTitle': 'CybertechDF',
        'liveBroadcastContent': 'none'
        }
        },
        {
        'kind': 'youtube#searchResult',
        'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/3kkXtgMMkGPfWQxyi1M88ZeiEjM"',
        'id':
        {
        'kind': 'youtube#playlist',
        'playlistId': 'PL442FA2C127377F07'
        },
        'snippet':
        {
        'publishedAt': '2011-09-25T04:59:31.000Z',
        'channelId': 'UCJbPGzawDH1njbqV-D5HqKw',
        'title': 'PHP Tutorials Playlist',
        'description': 'thenewboston Official Beginner PHP Tutorials Playlist!',
        'thumbnails':
        {
        'default':
        {
        'url': 'https://i.ytimg.com/vi/iCUV3iv9xOs/default.jpg',
        'width': 120,
        'height': 90
        },
        'medium':
        {
        'url': 'https://i.ytimg.com/vi/iCUV3iv9xOs/mqdefault.jpg',
        'width': 320,
        'height': 180
        },
        'high':
        {
        'url': 'https://i.ytimg.com/vi/iCUV3iv9xOs/hqdefault.jpg',
        'width': 480,
        'height': 360
        }
        },
        'channelTitle': 'thenewboston',
        'liveBroadcastContent': 'none'
        }
        },
        {
            'kind': 'youtube#searchResult',
            'etag': '"XpPGQXPnxQJhLgs6enD_n8JR4Qk/PLfog-iLp-oVm_Qbyf-l2rYFTMA"',
            'id':
            {
                'kind': 'youtube#video',
                'videoId': 'EDvE72Y4ckU'
            },
            'snippet':
            {
                'publishedAt': '2011-10-13T23:18:53.000Z',
                'channelId': 'UCfMb9oTGBoYNxy_1QrGWLIw',
                'title': 'Why Bucky Dropped Out of College - 82 - thenewboston Live!',
                'description': 'Visit my website at https://thenewboston.com/ for all my videos and tutorials! Check out the forum at https://thenewboston.com/forum/ ~ Donations ~ To support ...',
                'thumbnails':
                {
                    'default':
                    {
                        'url': 'https://i.ytimg.com/vi/EDvE72Y4ckU/default.jpg',
                        'width': 120,
                        'height': 90
                    },
                    'medium':
                    {
                        'url': 'https://i.ytimg.com/vi/EDvE72Y4ckU/mqdefault.jpg',
                        'width': 320,
                        'height': 180
                    },
                    'high':
                    {
                        'url': 'https://i.ytimg.com/vi/EDvE72Y4ckU/hqdefault.jpg',
                        'width': 480,
                        'height': 360
                    }
                },
                'channelTitle': 'Bucky Roberts',
                'liveBroadcastContent': 'none'
            }
        }
    ]
}