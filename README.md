
# ZenTube

## About
YouTube in Zen-mode.

### Features: 
 - No distracting areas (titlebars, headers, etc).
 - Search and watch YouTube videos online. 
 - Download YouTube videos to watch offline.
 
## Requirements:

Python (Version 3 preferred)

Google APIs Client Library for Python:

`pip install --upgrade google-api-python-client`

<!-- For Auth: `pip install --upgrade google-auth google-auth-oauthlib google-auth-httplib2` -->

## Available Scripts

To use **ZenTube**, run the following commands:

```
cd /path/to/project/directory
python zentube.py
```