import webview
import argparse

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# from misc.Dict import Dict

# To Download
# from __future__ import unicode_literals
# import youtube_dl

# ydl_opts = {}
# with youtube_dl.YoutubeDL(ydl_opts) as ydl:
# ydl.download(['https://www.youtube.com/watch?v=dP15zlyra3c'])

import logging
logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)
import math

DEVELOPER_KEY               = 'AIzaSyCcTZnhCe59st-2o10NMVNnc8Nq-TJJZ9U'
YOUTUBE_API_SERVICE_NAME    = 'youtube'
YOUTUBE_API_VERSION         = 'v3'

def api(some_fn):
    def to_exec(*args, **kwargs):
        args = tuple(arg for arg in args if arg is not None)
        some_fn(*args, **kwargs)
    return to_exec

class Html():
    def __init__(self):
        pass

    def get_col(self, content, **kwargs):
        classes = ' '.join(kwargs['classes']) if 'classes' in kwargs.keys() else ''
        # return '<div class="col{} {}">{}</div>'.format(" s" + str(col_size) if type(col_size) is int and col_size>0 and col_size<=12 else '', classes, content)
        return '<div class="col {}">{}</div>'.format(classes, content)

    def get_row(self, content, **kwargs):
        classes = ' '.join(kwargs['classes']) if 'classes' in kwargs.keys() else ''
        return '<div class="row {}">{}</div>'.format(classes, content)

    def get_playbox(self, content):
        return  ('<div class="row pb-2 playbox" data-kind="{kind}" data-id="{id}" data-channel-id="{channel_id}">'
                    '<div class="col s2">'
                        '<a class="play_content"><img src="{img}" class="responsive-img" /></a>'
                    '</div>'
                    '<div class="col s10">'
                        '<h5 class="title"><a class="play_content">{title}</a></h5>'
                        '<h6><a class="show_channel">{channel_title}</a></h6>'
                        '<p class="truncate white-text">{description}</p>'
                    '</div>'
                '</div>').format(   img=content['img'], 
                                    id=content['id'], 
                                    title=content['title'], 
                                    channel_id=content['channel_id'], 
                                    channel_title=content['channel_title'], 
                                    description=content['description'],
                                    kind=content['kind']   )

    def get_channel_box(self, content):
        # return ''
        pass

    def get_next_page(self, next_page_token):
        return ('<div class="row">'
                    '<div class="col s12 center-align">'
                        '<button class="load_more waves-effect waves-light btn red accent-4" data-next-page-token="{}"><i class="material-icons right">more</i>Load More</button>'
                    '</div>'
                '</div>').format(next_page_token)

    def get_default_page(self):
        return ('<div class="row center-align">'
                    '<div class="col s12 pb-2 white-text valign-wrapper">'
                        '<span>Unable to load. Please check your Internet Connectivity. <i class="material-icons right">network_check</i></span>'
                    '</div>'
                    '<div class="col s12 pb-2 white-text">'
                        '<button class="retry_connection waves-effect waves-light btn red accent-4">Retry<i class="material-icons right">refresh</i></button>'
                    '</div>'
                '</div>')


class WebviewApi():
    def __init__(self):
        self.html = Html()
        self.youtube = None
        self.try_connection()

    def print_js(self, params):
        print("----------------")
        print("Print from JS")
        print(params)
        print("----------------") 

    @api
    def try_connection(self):
        try:
            if self.youtube is None:
                self.youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=DEVELOPER_KEY)
        except Exception:
            self.youtube = None

        # return self.youtube

    def yt_search(self, args):
        args = args if args else {'q': 'Sujan Acharya', 'maxResults': 10}
        content = []
        try:
            if 'part' not in args.keys():
                args['part'] = 'snippet'
                # args['part'] = 'snippet,contentDetails' # Try this out

            if 'maxResults' not in args.keys():
                args['maxResults'] = 10

            search_response = self.youtube.search().list(**args).execute()
            for item in search_response['items']:
                if item['id']['kind']=="youtube#video" or item['id']['kind']=="youtube#playlist":
                    playbox =   {
                                    'img': item['snippet']['thumbnails']['high']['url'],
                                    'title': item['snippet']['title'],
                                    'description': item['snippet']['description'],
                                    'channel_id': item['snippet']['channelId'],
                                    'channel_title': item['snippet']['channelTitle'],
                                }

                    if item['id']['kind']=="youtube#video":
                        playbox['id'] = item['id']['videoId']
                        playbox['kind'] = "video"
                    elif item['id']['kind']=="youtube#playlist":
                        playbox['id'] = item['id']['playlistId']
                        playbox['kind'] = "playlist"

                    content.append(self.html.get_playbox(playbox))

            content.append(self.html.get_next_page(search_response['nextPageToken']))

        except HttpError as e_http:
            print ('An HTTP error {} occurred:\n{}'.format(e_http.resp.status, e_http.content))
            content.append(self.html.get_default_page())
            self.youtube = None
        except Exception as e_general:
            print("Some Exception occured:", e_general, sep="\n")
            content.append(self.html.get_default_page())
            self.youtube = None

        content = ''.join([row for row in content])
        return content

    @api
    def toggleFullscreen(self):
        webview.toggle_fullscreen()


if __name__ == "__main__":
    api = WebviewApi()
    webview.create_window("ZenTube", "assets/index.html", js_api=api)